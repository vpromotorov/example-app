import axios from '../index';
import {AxiosResponse} from "axios";

export interface JokeResponseI {
    icon_url: string;
    id: string;
    url: string;
    value: string;
}


export function getRandomJoke(): Promise<AxiosResponse<JokeResponseI>> {
    return axios.get('/jokes/random');
}