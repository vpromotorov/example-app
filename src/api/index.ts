import axios from 'axios';

const config = axios.create({
    baseURL: `https://api.chucknorris.io`,
    timeout: 10000,
});

config.defaults.headers.common['Content-Type'] = 'application/json';

export default config;
