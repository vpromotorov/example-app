import {configureStore} from "@reduxjs/toolkit";
import jokesSlice from "../modules/jokes/reducer";

export const store = configureStore({
    reducer: {
        jokes: jokesSlice.reducer
    }
});

export type RootStateI = ReturnType<typeof store.getState>;