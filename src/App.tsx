import React, {useEffect} from 'react';
import {useDispatch} from 'react-redux'
import './App.css';
import {BrowserRouter} from "react-router-dom";
import {AppRouter} from "./pages";
import jokesSlice from "./modules/jokes/reducer";
import {getFavoriteJokes} from "./shared/services/jokeStorage";

function App() {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(jokesSlice.actions.setFavorite(getFavoriteJokes()));
  }, [dispatch]);

  return (
    <BrowserRouter>
      <AppRouter />
    </BrowserRouter>
  );
}

export default App;
