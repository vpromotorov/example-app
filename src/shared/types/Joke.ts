import {JokeResponseI} from "../../api/requests/joke";

export interface ExtendedJokeI extends JokeResponseI {
    isFavorite: boolean;
}