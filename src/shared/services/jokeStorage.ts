import {ExtendedJokeI} from "../types/Joke";

const favoriteJokesKey = 'favoriteJokes';

export function setFavoriteJokes(jokes: ExtendedJokeI[]): void {
    localStorage.setItem(favoriteJokesKey, JSON.stringify(jokes));
}

export function getFavoriteJokes(): ExtendedJokeI[] {
    const data = localStorage.getItem(favoriteJokesKey);
    return data ? JSON.parse(data) : [];
}