import React, {useCallback} from 'react';
import {RootStateI} from "../../../store";
import {ExtendedJokeI} from "../../../shared/types/Joke";
import {useDispatch, useSelector} from "react-redux";
import TaskItem from "../smartComponents/JokeItem";
import {Link} from "react-router-dom";
import {routes} from "../../../pages";
import jokesSlice from "../reducer";
import './styles.scss';

interface Props {}

const favoritesSelector = (state: RootStateI): ExtendedJokeI[] => state.jokes.favorite;

const JokesFavorites: React.FC<Props> = () => {
  const favorites = useSelector(favoritesSelector);
  const dispatch = useDispatch();

  const clear = useCallback(() => {
    dispatch(jokesSlice.actions.clearFavorite());
  }, [dispatch])

  return <div className="flex-column joke-favorites-wrapper">
    <div className="flex-space-between flex-wrap-container">
    {
      favorites.map(x => <TaskItem key={x.id} item={x} />)
    }
    </div>
    {
      favorites.length === 0 ? <span>Нет избранных</span> :  <button onClick={clear}>Очистить</button>
    }
    <Link to={routes.main}>На главную</Link>
  </div>
}

export default React.memo(JokesFavorites);
