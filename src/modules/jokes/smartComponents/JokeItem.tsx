import React, {useCallback} from 'react';
import {ExtendedJokeI} from "../../../shared/types/Joke";
import {useDispatch} from "react-redux";
import jokesSlice from "../reducer";

interface Props {
    item: ExtendedJokeI;
}

const JokeItem: React.FC<Props> = (props) => {
    const {item} = props;
    const dispatch = useDispatch();

    const favoriteButtonClick = useCallback(() => {
        const action = item.isFavorite ? jokesSlice.actions.removeFavorite : jokesSlice.actions.addFavorite;
        dispatch(action(item));
    }, [dispatch, item]);

    return <div className="joke-item-wrapper">
        <h1 style={{wordBreak: 'break-word'}}>№{item.id}</h1>
        <div className="flex-centered">
            <img src={item.icon_url} alt="joke icon" />
            <button onClick={favoriteButtonClick}>{item.isFavorite ? 'Удалить из избранных' : 'Добавить в избранные'}</button>
        </div>
        <div>
            {item.value}
        </div>
    </div>
}

export default React.memo(JokeItem);
