import React, {useCallback, useEffect, useRef, useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {fetchRandomJoke} from "../reducer";
import {RootStateI} from "../../../store";
import {ExtendedJokeI} from "../../../shared/types/Joke";
import TaskItem from "../smartComponents/JokeItem";
import {Link} from "react-router-dom";
import {routes} from "../../../pages";

interface Props {}

const displayedTaskSelector = (store: RootStateI): ExtendedJokeI | null => store.jokes.displayed;

const TaskMain: React.FC<Props> = () => {
    const [isAutoGeneration, setAutoGeneration] = useState(false);
    const autoGenerationRef = useRef(isAutoGeneration);
    const intervalRef = useRef<NodeJS.Timeout>();
    const dispatch = useDispatch();

    const displayedTask = useSelector(displayedTaskSelector);

    const showNewTask = useCallback(() => {
        dispatch(fetchRandomJoke());
    }, [dispatch]);

    const switchAutoGeneration = useCallback(() => {
        setAutoGeneration(!isAutoGeneration);
    }, [isAutoGeneration]);

    useEffect(() => {
      intervalRef.current = setInterval(() => {
          autoGenerationRef.current && dispatch(fetchRandomJoke());
      }, 3000);
      return () => {
          intervalRef.current && clearInterval(intervalRef.current);
      }
    }, [dispatch]);

    useEffect(() => {
        autoGenerationRef.current = isAutoGeneration;
    }, [isAutoGeneration]);

    return <div>
        {displayedTask && <TaskItem item={displayedTask} />}
        <button onClick={showNewTask}>Показать</button>
        <button onClick={switchAutoGeneration}>{isAutoGeneration ? 'Отключить автогенерацию' : 'Включить автогенерацию'}</button>
        <Link to={routes.favorites}>Показать любимые шутки</Link>
    </div>
}

export default React.memo(TaskMain);
