import {createAsyncThunk, createSlice, Draft, PayloadAction} from "@reduxjs/toolkit";
import {ExtendedJokeI} from "../../../shared/types/Joke";
import {getRandomJoke, JokeResponseI} from "../../../api/requests/joke";
import {setFavoriteJokes} from "../../../shared/services/jokeStorage";

interface JokesReducerStateI {
    favorite: ExtendedJokeI[];
    displayed: ExtendedJokeI | null;
}

const initialState: JokesReducerStateI = {
    favorite: [],
    displayed: null,
}

export const fetchRandomJoke = createAsyncThunk('getRandomJoke', async (): Promise<JokeResponseI> => {
   return (await getRandomJoke()).data;
});

const sharedReducer = (val: Draft<JokesReducerStateI>, isFavorite: boolean): void => {
    if (val.displayed) {
        val.displayed.isFavorite = isFavorite;
    }
    setFavoriteJokes(val.favorite);
}

const jokesSlice = createSlice({
    name: 'jokes',
    initialState,
    reducers: {
        addFavorite: (state, action: PayloadAction<ExtendedJokeI>) => {
            const newJoke = {
                ...action.payload,
                isFavorite: true,
            };
            if (state.favorite.length >= 10) {
                state.favorite.splice(0, 1);
            }
            state.favorite.push(newJoke);
            sharedReducer(state, true);
        },
        removeFavorite: (state, action: PayloadAction<ExtendedJokeI>) => {
            const index = state.favorite.findIndex(x => x.id === action.payload.id);
            if (index !== -1) state.favorite.splice(index, 1);
            sharedReducer(state, false);
        },
        clearFavorite: (state) => {
            state.favorite = [];
            sharedReducer(state, false);
        },
        setFavorite: (state, action: PayloadAction<ExtendedJokeI[]>) => {
            state.favorite = action.payload;
        }
    },
    extraReducers: builder => {
        builder.addCase(fetchRandomJoke.fulfilled, (state, action: PayloadAction<JokeResponseI>) => {
            const newJoke = action.payload;
            state.displayed = {
                ...newJoke,
                isFavorite: state.favorite.some(x => x.id === newJoke.id),
            };
        });
    },
});

export default jokesSlice;