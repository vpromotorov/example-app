import React from 'react';
import { Switch, Route } from 'react-router-dom';
import TaskMain from "../modules/jokes/jokeMain";
import TaskFavorites from "../modules/jokes/jokeFavorites";

export const routes = {
    main: '/',
    favorites: '/favorites',
}

export function AppRouter() {
    return (
        <div>
            <Switch>
                <Route path={routes.main} component={TaskMain} exact />
                <Route path={routes.favorites} component={TaskFavorites} />
            </Switch>
        </div>
    );
}